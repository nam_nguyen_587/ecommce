package com.example.demo.controllers;

import com.example.demo.model.persistence.User;
import com.example.demo.model.persistence.UserOrder;
import com.example.demo.model.persistence.repositories.OrderRepository;
import com.example.demo.model.persistence.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static com.example.demo.controllers.Utils.createItems;
import static com.example.demo.controllers.Utils.createUser;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class OrderControllerTest {

    @InjectMocks
    OrderController orderController;

    @Mock
    private UserRepository userRepository;

    @Mock
    private OrderRepository orderRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        User user = createUser();
        when(userRepository.findByUsername("udacity")).thenReturn(user);
        when(orderRepository.findByUser(any())).thenReturn(Utils.createOrders());
    }

    @Test
    public void verify_submit() {

        ResponseEntity<UserOrder> response = orderController.submit("udacity");
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        UserOrder order = response.getBody();

        assertEquals(createItems(), order.getItems());
        assertEquals(createUser().getId(), order.getUser().getId());


        verify(orderRepository, times(1)).save(order);

    }

    @Test
    public void verify_submitInvalid() {

        ResponseEntity<UserOrder> response = orderController.submit("pop");
        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue());

        assertNull(response.getBody());

        verify(userRepository, times(1)).findByUsername("pop");
    }

    @Test
    public void verify_getOrdersForUser() {

        ResponseEntity<List<UserOrder>> response = orderController.getOrdersForUser("udacity");
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        List<UserOrder> orders = response.getBody();


        assertEquals(Utils.createOrders().size(), orders.size());

    }

}